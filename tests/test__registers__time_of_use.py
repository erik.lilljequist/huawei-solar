from huawei_solar.registers import TimeOfUseRegisters
from huawei_solar.registers import HUAWEI_LUNA2000_TimeOfUsePeriod
from huawei_solar.registers import LG_RESU_TimeOfUsePeriod
from huawei_solar.registers import BinaryPayloadBuilder
from huawei_solar.exceptions import EncodeError
import pytest

lg_resu_tou = LG_RESU_TimeOfUsePeriod(
    start_time=0,
    end_time=0,
    electricity_price=1
)
huawei_luna2000_tou = HUAWEI_LUNA2000_TimeOfUsePeriod(
    end_time=0,
    start_time=0,
    charge_flag=0,
    days_effective=[True, True, True, True, True, True, True]
)

tou_reg = TimeOfUseRegisters(None, None, None)


def test__encode__tou_periods__empty():
    with pytest.raises(expected_exception=TypeError):
        tou_reg.encode(builder=None, data=[])


def test__encode__tou_periods__wrong_type():
    value = tou_reg.encode(builder=None, data=[1])
    assert isinstance(value, EncodeError)


def test__encode__tou_periods__different_type():
    tou = [huawei_luna2000_tou, lg_resu_tou]
    value = tou_reg.encode(builder=None, data=tou)
    assert isinstance(value, EncodeError)


def test__encode__tou_periods__HUAWEI_LUNA2000():
    tou = [huawei_luna2000_tou]
    value = tou_reg.encode(builder=BinaryPayloadBuilder(), data=tou)
    assert isinstance(value, type(None))


def test__encode__tou_periods_of_same_types__HUAWEI_LUNA2000():
    tou = [huawei_luna2000_tou, huawei_luna2000_tou]
    value = tou_reg.encode(builder=BinaryPayloadBuilder(), data=tou)
    assert isinstance(value, type(None))


def test__encode__tou_periods_of_same_types__LG_RESU():
    tou = [lg_resu_tou, lg_resu_tou]
    value = tou_reg.encode(builder=BinaryPayloadBuilder(), data=tou)
    assert isinstance(value, type(None))
